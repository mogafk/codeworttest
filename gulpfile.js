var gulp = require('gulp');
var spritesmith = require('gulp.spritesmith');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var replace = require('gulp-replace');
var size = require('gulp-filesize');

gulp.task('sprite', function() {//не используются на данный момент
    var spriteData =
        gulp.src('static/gulpsprite/*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.css',
            }));

    spriteData.img.pipe(gulp.dest('static/images/')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('release/css/')); // путь, куда сохраняем стили
});

gulp.task('makecss', function(){
    return gulp.src(["bower_components/jqueryformstylermaster/jquery.formstyler.css", "dev/css/*"])
        .pipe(concat("style.css"))
        .pipe(autoprefixer())
        .pipe(gulp.dest('release/css'))
        .pipe(rename("style.min.css"))
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(size())
        .pipe(gulp.dest('release/css'));
});

gulp.task('makejs', function(){
    return gulp.src(["bower_components/jquery/jquery.min.js", "bower_components/jquery-placeholder/jquery.placeholder.js",
    "bower_components/jqueryformstylermaster/jquery.formstyler.js", "dev/js/main.js"])
        .pipe(concat("main.js"))
        .pipe(gulp.dest('release/js'))
        .pipe(rename("main.min.js"))
        .pipe(uglify())
        .pipe(gulp.dest('release/js'))
        .pipe(size())
});

gulp.task('mvIndex', function(){
    return gulp.src(['dev/index.html'])
        .pipe(gulp.dest('release'));
});
