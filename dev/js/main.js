$(document).ready(function(){
    $('input[type=radio]').styler();
    $('input[type=checkbox]').styler();
    $('select').styler();

    $('.person-radio').on('click', function(){
        $('.person-radio').removeClass("checked");
        $(this).addClass("checked");
    });
});